## trakt-torrentfreak-top-10

Add _Torrentfreak Top 10_ movies to your own Trakt list.

### How to run it:

1. Open config.py.example and follow the instructions there
2. Rename config.py.example to config.py
3. Install requirements

	pip install -r requirements.txt

4. Run the main script:

	python main.py


### Demo:

![demo](https://gitlab.com/tanel/trakt-torrentfreak-top-10/raw/master/demo.gif)

[https://asciinema.org/a/7e2unuahsgval9s00mg1tcg34](https://asciinema.org/a/7e2unuahsgval9s00mg1tcg34)